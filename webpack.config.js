const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  devtool: 'inline-source-map',
  devServer: {
    historyApiFallback: true,
    proxy: {
      '/authenticate/**': {
        target: 'http://middleware-githubstars.herokuapp.com',
        secure: false,
        changeOrigin: true
      },
      '/invalidate/**': {
        target: 'http://middleware-githubstars.herokuapp.com',
        secure: false,
        changeOrigin: true
      }
    },
    port: 8000
  },
  module: {
    rules: [
      require('./configs/webpack/loader.svg'),
      require('./configs/webpack/loader.sass'),
      require('./configs/webpack/loader.babel'),
      require('./configs/webpack/loader.eslint')
    ],
  },
  resolve: {
    alias: {
      pages: path.resolve(__dirname, 'src/pages/'),
      assets: path.resolve(__dirname, 'src/assets/'),
      domain: path.resolve(__dirname, 'src/domain/'),
      containers: path.resolve(__dirname, 'src/containers/'),
      components: path.resolve(__dirname, 'src/components/')
    }
  }
};
