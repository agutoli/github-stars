module.exports = {
  test: /\.js$/,
  exclude: /node_modules/,
  loader: "eslint-loader",
  options: {
    fix: false,
    quiet: true,
    failOnError: false,
    emitWarning: true
  }
}
