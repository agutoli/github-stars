import gql from 'graphql-tag';

export default gql`
mutation RemoveStarMutation($input: RemoveStarInput!) {
  removeStar(input: $input) {
    clientMutationId
  }
}
`
