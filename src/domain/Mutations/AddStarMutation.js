import gql from 'graphql-tag';

export default gql`
mutation AddStarMutation($input: AddStarInput!) {
  addStar(input: $input) {
    clientMutationId
  }
}
`
