const dependencies = {
  settings: require('../../../settings'),
}

export default function GetGithubAuthUrl(injection) {
  const { settings } = Object.assign({}, dependencies, injection)
  const {
    CLIENT_ID,
    API_SCOPE,
    API_CALLBACK_URL,
    API_AUTHORIZE_URL,
  } = settings
  return `${API_AUTHORIZE_URL}?client_id=${CLIENT_ID}&redirect_uri=${API_CALLBACK_URL}&scope=${API_SCOPE}`
}
