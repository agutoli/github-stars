const { expect } = require('chai')

const GetGithubAuthUrl = require('./GetGithubAuthUrl')

describe('GetGithubAuthUrl', () => {
  let response
  let dependencies

  beforeEach(async () => {
    dependencies = {
      settings: {
        CLIENT_ID: 'a',
        API_SCOPE: 'b',
        API_CALLBACK_URL: 'http://callback.com',
        API_AUTHORIZE_URL: 'http://auth.com',
      },
    }
    response = await GetGithubAuthUrl(dependencies)
  })

  it('create auth url with oauthscope', () => {
    expect(response).to.equal('http://auth.com?client_id=a&redirect_uri=http://callback.com&scope=b')
  })
})
