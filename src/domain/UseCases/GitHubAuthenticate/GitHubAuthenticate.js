const dependencies = {
  axios: require('axios'),
  localStorage,
}

export default function GitHubAuthenticate(code, injection) {
  const { axios, localStorage } = Object.assign({}, dependencies, injection)
  return axios.get(`/authenticate/${code}`)
    .then((res) => {
      if (!res.data) return
      // store token
      localStorage.setItem('api_token', res.data.token)
    })
}
