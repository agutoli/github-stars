const { expect } = require('chai')
const { mock } = require('sinon')

global.localStorage = {}

const GitHubAuthenticate = require('./GitHubAuthenticate')

describe('GitHubAuthenticate', () => {
  let code = 'some hash'
  let response
  let dependencies

  beforeEach(async () => {
    dependencies = {
      localStorage: {
        setItem: mock()
          .withArgs(`api_token`, 'my_token')
      },
      axios: {
        get: mock()
          .withArgs(`/authenticate/${code}`)
          .resolves({ data: { token: 'my_token' } })
      }
    }
    response = await GitHubAuthenticate(code, dependencies)
  })

  it('calls axios.get once', () => dependencies.axios.get.verify())
  it('calls localStorage.setItem once', () => dependencies.localStorage.setItem.verify())
  it('returns promise with token', () => expect(response).to.equal(undefined))
})
