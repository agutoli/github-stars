const { expect } = require('chai')
const { mock } = require('sinon')

global.localStorage = {}

const ResetGithubTokenAuthorization = require('./ResetGithubTokenAuthorization')

describe('ResetGithubTokenAuthorization', () => {
  let token = 'my_token'
  let response
  let dependencies

  beforeEach(async () => {
    dependencies = {
      localStorage: {
        getItem: mock()
          .withArgs(`api_token`)
          .returns(token),
        removeItem: mock()
          .withArgs(`api_token`)
      },
      axios: {
        get: mock()
          .withArgs(`/invalidate/${token}`)
          .resolves({ data: { token: 'my_token' } })
      }
    }
    response = await ResetGithubTokenAuthorization(dependencies)
  })

  it('calls axios.get once', () => dependencies.axios.get.verify())
  it('calls localStorage.getItem once', () => dependencies.localStorage.getItem.verify())
})
