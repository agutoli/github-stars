const dependencies = {
  axios: require('axios'),
  localStorage, // this makes sense, beleave me :)
}

export default function ResetGithubTokenAuthorization(injection) {
  const { axios, localStorage } = Object.assign({}, dependencies, injection)
  const token = localStorage.getItem('api_token')

  return axios.get(`/invalidate/${token}`)
    .then((res) => {
      localStorage.removeItem('api_token')
      return res
    })
}
