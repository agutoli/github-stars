import gql from 'graphql-tag'

export default gql`
query ($login: String!) {
  viewer {
    name
    avatarUrl
  },
  organization(login: $login) {
    url
    email
    name
    login
    location
    avatarUrl
    websiteUrl
  },
  user(login: $login) {
    url
    bio
    email
    name
    login
    company
    location
    avatarUrl
    websiteUrl
  },
  repositoryOwner(login: $login) {
    repositories(first: 20, orderBy: {field: STARGAZERS, direction: DESC}) {
      edges {
        node {
          id
          url
          name
          createdAt
          description
          resourcePath
          nameWithOwner
          viewerHasStarred
          stargazers(first: 10) {
            totalCount
          }
        }
      }
    }
  }
}
`
