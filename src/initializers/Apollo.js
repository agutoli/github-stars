import { ApolloLink } from 'apollo-link'
import { onError } from 'apollo-link-error'
import { ApolloClient } from 'apollo-client'
import { setContext } from 'apollo-link-context'
import { createHttpLink } from 'apollo-link-http'
import { withClientState } from 'apollo-link-state'
import { InMemoryCache } from 'apollo-cache-inmemory'

import { GRAPHQL_API_URL } from '../settings'

export default () => {
  const cacheMemory = new InMemoryCache()

  const httpLink = createHttpLink({
    uri: GRAPHQL_API_URL,
  })

  const authLink = setContext((_, { headers }) => {
    const token = localStorage.getItem('api_token')
    return {
      headers: Object.assign({}, headers, { authorization: `Bearer ${token}` }),
    }
  })

  const errorLink = onError(({ response, graphQLErrors }) => {
    if (graphQLErrors) {
      graphQLErrors.map(({ message, locations, path }) =>
        console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`))
      response.errors = null
    }
  })

  const stateLink = withClientState({
    cache: cacheMemory,
    resolvers: {
      Mutation: {
        updateNetworkStatus: (_, { isConnected }, { cache }) => {
          const data = {
            networkStatus: {
              __typename: 'NetworkStatus',
              isConnected,
            },
          };
          cache.writeData({ data });
          return null
        },
      },
    },
  })

  return new ApolloClient({
    cache: cacheMemory,
    link: ApolloLink.from([
      stateLink,
      authLink,
      errorLink,
      httpLink,
    ]),
  })
}
