const settings = {
  API_SCOPE: 'user,public_repo,read:org',
  API_AUTHORIZE_URL: 'https://github.com/login/oauth/authorize',
  GRAPHQL_API_URL: 'https://api.github.com/graphql',
}

settings.development = {
  CLIENT_ID: '9909c09332aab512da10',
  API_CALLBACK_URL: 'http://localhost:8000/callback',
}

settings.production = {
  CLIENT_ID: 'c8d4481ae72675a6ce3b',
  API_CALLBACK_URL: 'http://bruno-agutoli-gitstars.herokuapp.com/callback',
}
console.log(process.env.NODE_ENV);
export default Object.assign({}, settings, settings[process.env.NODE_ENV])
