import React from 'react'
import PropTypes from 'prop-types'
import BEMHelper from 'react-bem-helper'

import './Alert.scss'

const BEM = new BEMHelper('alert')

function Alert({ message, onDismiss }) {
  return (
    <div {...BEM()}>
      {message} <span onClick={onDismiss}>dismiss</span>
    </div>
  )
}

Alert.propTypes = {
  message: PropTypes.string,
  onDismiss: PropTypes.func,
}

export default Alert
