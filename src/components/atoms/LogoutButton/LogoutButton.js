import React from 'react'
import PropTypes from 'prop-types'

import BEMHelper from 'react-bem-helper'

import './LogoutButton.scss'

const BEM = new BEMHelper('logout-button')

function LogoutButton({ onClick }) {
  return (
    <button {...BEM()} onClick={onClick}>
      Logout
    </button>
  )
}

LogoutButton.propTypes = {
  onClick: PropTypes.func,
}

export default LogoutButton
