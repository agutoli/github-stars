import React from 'react'
import PropTypes from 'prop-types'
import BEMHelper from 'react-bem-helper'

import './StarButton.scss'

const BEM = new BEMHelper('star-button')

class StarButton extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: false,
    }

    this.onClickBinded = this.onClick.bind(this)
  }

  componentWillReceiveProps() {
    this.setState({ isLoading: false })
  }

  onClick() {
    const { onClick } = this.props
    this.setState({ isLoading: true })
    onClick()
  }

  render() {
    const { starred } = this.props
    const { isLoading } = this.state
    return (
      <button {...BEM({ modifiers: { starred, loading: isLoading } })} onClick={this.onClickBinded}>
        { isLoading ? 'wait...' : starred ? 'unstar' : 'star' }
      </button>
    )
  }
}

StarButton.propTypes = {
  starred: PropTypes.bool,
  onClick: PropTypes.any,
}

export default StarButton
