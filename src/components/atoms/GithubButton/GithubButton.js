import React from 'react'
import PropTypes from 'prop-types'

import BEMHelper from 'react-bem-helper'
import GithubIcon from 'assets/svgs/github.svg'

import './GithubButton.scss'

const BEM = new BEMHelper('github-button')

function GithubButton({ url, className }) {
  return (
    <a href={url} {...BEM({ extra: [className] })}>
      <GithubIcon {...BEM('github-icon')} />
      Sign in with Github
    </a>
  )
}

GithubButton.propTypes = {
  url: PropTypes.string,
  className: PropTypes.string,
}

export default GithubButton
