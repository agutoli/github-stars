import React from 'react'
import PropTypes from 'prop-types'

import BEMHelper from 'react-bem-helper'

import './Thumbnail.scss'

const BEM = new BEMHelper('thumbnail')

class Thumbnail extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      isLoaded: false,
    }
  }

  render() {
    const {
      url,
      alt,
      height,
      width,
      className,
    } = this.props
    const { isLoaded } = this.state
    return (
      <div
        style={{ width, height }}
        {...BEM({ extra: [className], modifiers: { loaded: isLoaded } })}>
        <img
          src={url}
          alt={alt}
          height={height}
          width={width}
          onLoad={() => this.setState({ isLoaded: true })} />
      </div>
    )
  }
}

Thumbnail.propTypes = {
  url: PropTypes.string,
  alt: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  className: PropTypes.string,
}

export default Thumbnail
