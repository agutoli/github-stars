import React from 'react'
import PropTypes from 'prop-types'

import { withRouter } from 'react-router-dom'
import BEMHelper from 'react-bem-helper'
import SearchIcon from 'assets/svgs/search.svg'

import './InputSearch.scss'

const BEM = new BEMHelper('input-search')

class InputSearch extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      hasError: false,
      searchTerm: '',
    }

    // Use bind is better than arrow funcitons
    this.onSearchChangeBinded = this.onSearch.bind(this)
    this.onSearchDispachBinded = this.onSearchDispach.bind(this)
  }

  onSearch(e) {
    this.setState({ searchTerm: e.target.value })
  }

  onSearchDispach(e) {
    const { searchTerm } = this.state

    if (e.key !== 'Enter' && e.type !== 'click') {
      return
    }

    if (!searchTerm) {
      this.setState({ hasError: true })
      return
    }

    if (e.key === 'Enter') {
      this.props.history.push(`/search/${searchTerm}`)
    }

    if (e.type === 'click') {
      this.props.history.push(`/search/${searchTerm}`)
    }

    this.setState({ hasError: false })
  }

  render() {
    const { large, small, className } = this.props
    const { hasError } = this.state

    return (
      <label
        htmlFor="input-search"
        {...BEM({ modifiers: { large, small, error: hasError }, extra: [className] })}>
        <input
          {...BEM('input')}
          id="input-search"
          placeholder="github username..."
          onKeyPress={this.onSearchDispachBinded}
          onChange={this.onSearchChangeBinded} />
        <SearchIcon
          {...BEM('search-icon')}
          onClick={this.onSearchDispachBinded} />
      </label>
    )
  }
}

InputSearch.propTypes = {
  large: PropTypes.bool,
  small: PropTypes.bool,
  className: PropTypes.string,
  history: PropTypes.object,
}

export default withRouter(InputSearch)
