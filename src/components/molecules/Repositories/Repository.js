import React from 'react'
import PropTypes from 'prop-types'

import BEMHelper from 'react-bem-helper'

import StarIcon from 'assets/svgs/star.svg'
import StarButton from 'components/atoms/StarButton'

import './Repository.scss'

const BEM = new BEMHelper('repository')

function Repository(props) {
  const { node, addStar, removeStar } = props
  const starAction = () => node.viewerHasStarred ? removeStar() : addStar()

  return (
    <div {...BEM()}>
      <div {...BEM('left')}>
        <h2 {...BEM('repo-name')}>{node.nameWithOwner}</h2>
        <p {...BEM('description')}>{node.description}</p>
        <div key={node.stargazers.totalCount} {...BEM('star')}><StarIcon />
          <span>{node.stargazers.totalCount}</span>
        </div>
      </div>
      <div key={node.stargazers.totalCount} {...BEM('right')} >
        <StarButton
          starred={node.viewerHasStarred}
          onClick={starAction} />
      </div>
    </div>
  )
}

Repository.propTypes = {
  node: PropTypes.object,
  addStar: PropTypes.func,
  removeStar: PropTypes.func,
  repositories: PropTypes.shape({
    edges: PropTypes.array,
  }),
}

Repository.defaultProps = {
  node: {},
  repositories: {
    edges: [],
  },
  addStar: () => {},
  removeStar: () => {},
}

export default Repository
