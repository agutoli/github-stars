import React from 'react'
import PropTypes from 'prop-types'

import BEMHelper from 'react-bem-helper'

import './Repositories.scss'

const BEM = new BEMHelper('repositories')

function Repositories(props) {
  return (
    <div {...BEM()}>{props.children}</div>
  )
}

Repositories.propTypes = {
  children: PropTypes.any,
}

export default Repositories
