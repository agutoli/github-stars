import React from 'react'
import PropTypes from 'prop-types'
import BEMHelper from 'react-bem-helper'

import EmailIcon from 'assets/svgs/email.svg'
import CompanyIcon from 'assets/svgs/company.svg'
import WebsiteIcon from 'assets/svgs/website.svg'
import LocationIcon from 'assets/svgs/location.svg'

import './UserProfile.scss'

const BEM = new BEMHelper('user-profile')

function UserProfile({ user }) {
  return (
    <div {...BEM()}>
      <div {...BEM('hero')}>
        <img src={user.avatarUrl} {...BEM('avatar')} alt={user.name} />
        <h2 {...BEM('name')}>{user.name}</h2>
        <h3 {...BEM('login')}>{user.login}</h3>
      </div>
      <div {...BEM('info')}>
        <p {...BEM('bio')}>{user.bio}</p>
        <p {...BEM('section')}><CompanyIcon /> {user.company ? user.company : 'no company'}</p>
        <p {...BEM('section')}><LocationIcon /> {user.location ? user.location : 'no location'}</p>
        <p {...BEM('section')}><EmailIcon /> {user.email ? user.email : 'no email'}</p>
        <p {...BEM('section')}><WebsiteIcon /> {user.websiteUrl ? user.websiteUrl : 'no website url'}</p>
      </div>
    </div>
  )
}

UserProfile.propTypes = {
  user: PropTypes.object,
}

UserProfile.defaultProps = {
  user: {},
}

export default UserProfile
