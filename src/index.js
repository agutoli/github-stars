import React from 'react'
import ReactDOM from 'react-dom'
import GitHubAuthenticate from 'domain/UseCases/GitHubAuthenticate'

import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo'

// Pages
import HomePage from 'containers/Home'
import LogoutButton from 'containers/LogoutButton'
import SearchResultPage from 'containers/SearchResult'

import ApolloInitializer from './initializers/Apollo'

import './app.scss'

function getTokenCallback() {
  const code = window.location.href.match(/\?code=(.*)/)[1]
  GitHubAuthenticate(code)
    .then(() => {
      window.location.href = '/'
    })
  return <div />
}

const client = ApolloInitializer()

const App = function App() {
  return (
    <ApolloProvider client={client}>
      <Router>
        <div>
          <LogoutButton />
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route
              exact
              path="/search/:username"
              component={({ match }) => <SearchResultPage login={match.params.username} />} />
            <Route exact path="/callback" component={getTokenCallback} />
            <Route component={() => <Redirect to="/" />} />
          </Switch>
        </div>
      </Router>
    </ApolloProvider>
  )
}

ReactDOM.render(<App />, document.getElementById('main'))
