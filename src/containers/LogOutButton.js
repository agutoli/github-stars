import React from 'react'
import { withRouter } from 'react-router-dom'
import ResetGithubTokenAuthorization from 'domain/UseCases/ResetGithubTokenAuthorization'

import LogoutButton from 'components/atoms/LogoutButton'

export default withRouter(({ history }) => (
  <LogoutButton onClick={() => {
    ResetGithubTokenAuthorization()
      .then(() => {
        history.push('/')
        window.location.reload()
      })
  }} />
))
