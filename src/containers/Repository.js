import { graphql, compose } from 'react-apollo'

import SearchQuery from 'domain/Queries/SearchQuery'
import AddStarMutation from 'domain/Mutations/AddStarMutation'
import RemoveStarMutation from 'domain/Mutations/RemoveStarMutation'

import Repository from 'components/molecules/Repositories/Repository'

function updateAfterMutation({ login, onStarred, onUnstarred }) {
  return ({
    errorPolicy: 'all',
    refetchQueries: ({ data }) => {
      if (data.addStar) {
        onStarred()
      }

      if (data.removeStar) {
        onUnstarred()
      }

      return [
        {
          query: SearchQuery,
          variables: { login },
        },
      ]
    },
  })
}

export default compose(
  graphql(AddStarMutation, {
    name: 'addStar',
    options: updateAfterMutation,
  }),
  graphql(RemoveStarMutation, {
    name: 'removeStar',
    options: updateAfterMutation,
  }),
)(Repository)
