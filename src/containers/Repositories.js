import React from 'react'
import PropTypes from 'prop-types'

import Alert from 'components/atoms/Alert'
import Repositories from 'components/molecules/Repositories'

import Repository from './Repository'

class RepositoriesContainer extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      alertText: '',
      isShowAlert: false,
    }

    this.onStarredBinded = this.onStarred.bind(this)
    this.onUnstarredBinded = this.onUnstarred.bind(this)
  }

  onStarred() {
    this.showAlertTimeout('Repo starred with success!')
  }

  onUnstarred() {
    this.showAlertTimeout('Repo unstarred with success!')
  }

  showAlertTimeout(alertText) {
    this.setState({
      alertText,
      isShowAlert: true,
    })
    window.setTimeout(() => {
      this.setState({
        alertText: '',
        isShowAlert: false,
      })
    }, 2000)
  }

  render() {
    const { isShowAlert, alertText } = this.state
    const { repositories, user } = this.props
    return (
      <Repositories>
        {repositories.edges.map(item => (
          <Repository
            {...item}
            onStarred={this.onStarredBinded}
            onUnstarred={this.onUnstarredBinded}
            key={item.node.id}
            login={user.login}
            input={{ starrableId: item.node.id }}
          />
        ))}
        { isShowAlert ? <Alert message={alertText} /> : null }
      </Repositories>
    )
  }
}

RepositoriesContainer.propTypes = {
  user: PropTypes.object,
  repositories: PropTypes.shape({
    edges: PropTypes.array,
  }),
}

RepositoriesContainer.defaultProps = {
  user: {},
  repositories: {
    edges: [],
  },
}

export default RepositoriesContainer
