import { graphql } from 'react-apollo'

import SearchQuery from 'domain/Queries/SearchQuery'

import SearchResultPage from 'pages/SearchResult'

function updateAfterMutation({ login }) {
  if (!login) {
    return {}
  }
  return ({
    errorPolicy: 'all',
    refetchQueries: [
      {
        query: SearchQuery,
        variables: { login },
      },
    ],
  })
}

export default graphql(SearchQuery, {
  name: 'data',
  options: updateAfterMutation,
})(SearchResultPage)
