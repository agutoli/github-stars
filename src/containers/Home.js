import { graphql } from 'react-apollo'
import AuthenticatedUserQuery from 'domain/Queries/AuthenticatedUserQuery'
import HomePage from 'pages/Home'

export default graphql(AuthenticatedUserQuery)(HomePage)
