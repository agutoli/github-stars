import React from 'react'
import PropTypes from 'prop-types'

import Logo from 'assets/svgs/logo.svg'
import { BounceLoader } from 'react-spinners'
import InputSearch from 'components/atoms/InputSearch'
import GithubButton from 'components/atoms/GithubButton'
import GetGithubAuthUrl from 'domain/UseCases/GetGithubAuthUrl'


import './Home.scss'

function HomePage({ data }) {
  return (
    <div className="home">
      <Logo className="home__logo" />
      { data.loading ? <BounceLoader size={90} color="#ccc" /> :
        data.viewer ?
          <InputSearch large className="home__input-search" /> :
          <GithubButton url={GetGithubAuthUrl()} className="home__github-button" />
      }
    </div>
  )
}

HomePage.propTypes = {
  data: PropTypes.shape({
    viewer: PropTypes.object,
  }),
}

HomePage.defaultProps = {
  data: {
    viewer: null,
  },
}

export default HomePage
