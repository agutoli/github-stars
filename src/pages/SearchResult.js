import React from 'react'
import PropTypes from 'prop-types'
import BEMHelper from 'react-bem-helper'

import Logo from 'assets/svgs/logo.svg'
import ErrorIcon from 'assets/svgs/error.svg'
import Repositories from 'containers/Repositories'
import Thumbnail from 'components/atoms/Thumbnail'
import InputSearch from 'components/atoms/InputSearch'
import UserProfile from 'components/molecules/UserProfile'

import './SearchResult.scss'

const BEM = new BEMHelper('search-results')

function ErrorMessage() {
  return (
    <div {...BEM('error')}>
      <ErrorIcon />
      <p>User not found</p>
    </div>
  )
}

function SearchResult({ data }) {
  const user = data.user ? data.user : data.organization
  const errors = data.error ? data.error.graphQLErrors.map(x => x.path[0]) : []

  const showErrorPage = !user && (errors.includes('user') && errors.includes('organization'))

  return (
    <div {...BEM({ extra: ['container'] })}>
      <div {...BEM('header')}>
        <Logo {...BEM('logo')} />
        <InputSearch small {...BEM('input-search')} />
        <Thumbnail
          width={64}
          height={64}
          alt={data.viewer ? data.viewer.name : null}
          url={data.viewer ? data.viewer.avatarUrl : null} />
      </div>
      <div {...BEM('content', { loaded: !data.loading })}>
        { showErrorPage ? <ErrorMessage /> : null }
        { user ? <UserProfile user={user} /> : null }
        { user ? <Repositories user={user} {...data.repositoryOwner} /> : null }
      </div>
    </div>
  )
}

SearchResult.propTypes = {
  data: PropTypes.shape({
    user: PropTypes.object,
    organization: PropTypes.object,
    repositoryOwner: PropTypes.object,
  }),
}

SearchResult.defaultProps = {
  data: {
    user: null,
    organization: null,
    repositoryOwner: null,
  },
}

export default SearchResult
